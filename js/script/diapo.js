const columns = Array.from(document.querySelectorAll(".column"));
// const lenghtDiapo = Array.from(columns[0].querySelectorAll(".img-wrap"));
let counters = [1, 2, 3];

const changeHtml = (numColumn, diapoImages) => {
  if (counters[numColumn] === diapoImages.length) {
    diapoImages[diapoImages.length - 1].classList.remove("show");
    diapoImages[1].classList.remove("show");
    diapoImages[0].classList.add("show");
    counters[numColumn] = 1;
  } else {
    if (diapoImages[counters[numColumn] - 1]) {
      diapoImages[counters[numColumn] - 1].classList.remove("show");
    }
    if (diapoImages[counters[numColumn] + 1]) {
      diapoImages[counters[numColumn] + 1].classList.remove("show");
    }
    diapoImages[counters[numColumn]].classList.add("show");
    counters[numColumn]++;
  }
};

const setIntervalDependOnColumn = (numColumn, diapoImages) => {
  setInterval(() => {
    changeHtml(numColumn, diapoImages);
  }, 6000);
};

const start = () => {
  columns.forEach((column) => {
    const diapoImages = Array.from(column.querySelectorAll(".img-wrap"));
    if (columns[0] === column) {
      setIntervalDependOnColumn(0, diapoImages);
    } else if (columns[1] === column) {
      setIntervalDependOnColumn(1, diapoImages);
    } else {
      setIntervalDependOnColumn(2, diapoImages);
    }
  });
};

document.querySelectorAll(".column3 .img-description").forEach((img) => {
  img.addEventListener("click", (e) => {
    let newNounters;
    if (counters[0] > 1) {
      counters = counters.map((counter) => (counter = counter - 2));
    } else {
      console.log("no");
    }

    columns.forEach((column) => {
      const diapoImages = Array.from(column.querySelectorAll(".img-wrap"));
      if (columns[0] === column) {
        changeHtml(0, diapoImages);
      } else if (columns[1] === column) {
        changeHtml(1, diapoImages);
      } else {
        changeHtml(2, diapoImages);
      }
    });
  });
});

document.querySelectorAll(".column1 .img-description").forEach((img) => {
  img.addEventListener("click", (e) => {
    columns.forEach((column) => {
      const diapoImages = Array.from(column.querySelectorAll(".img-wrap"));
      if (columns[0] === column) {
        changeHtml(0, diapoImages);
      } else if (columns[1] === column) {
        changeHtml(1, diapoImages);
      } else {
        changeHtml(2, diapoImages);
      }
    });
  });
});

start();
