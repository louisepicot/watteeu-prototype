let closeCate = false;
let closeAbout = false;
let closeDetail = false;
document.querySelector(".show-category > h2").addEventListener("click", (e) => {
  document.querySelector(".category").classList.toggle("show");
  closeCate = !closeCate;
  if (closeCate && !closeAbout) {
    document.querySelector(".show-category > h2").innerHTML = "Close";
  } else if (closeAbout && closeCate) {
    document.querySelector(".about").classList.remove("show");
    document.querySelector(".show-about > h2").innerHTML = "About";
    closeAbout = false;
    document.querySelector(".show-category > h2").innerHTML = "Close";
  } else {
    document.querySelector(".show-category > h2").innerHTML = "Category";
  }
});

document.querySelector(".show-about > h2").addEventListener("click", (e) => {
  document.querySelector(".about").classList.toggle("show");
  closeAbout = !closeAbout;
  if (closeAbout && !closeCate & !closeDetail) {
    document.querySelector(".show-about > h2").innerHTML = "Close";
  } else if (closeAbout && closeCate) {
    document.querySelector(".category").classList.remove("show");
    document.querySelector(".show-category > h2").innerHTML = "Category";
    closeCate = false;
    document.querySelector(".show-about > h2").innerHTML = "Close";
  } else if (closeDetail) {
    document.querySelector(".details").classList.toggle("show");
    closeDetail = !closeDetail;
    document.querySelector(".about").classList.toggle("show");
    closeAbout = !closeAbout;
    document.querySelector(".show-about > h2").innerHTML = "About";
    document
      .querySelectorAll(".index .column")
      .forEach((column) => column.classList.toggle("small-column"));
  } else {
    document.querySelector(".show-about > h2").innerHTML = "About";
  }
});

const allFurnitures = document.querySelectorAll(".index .img-wrap");
allFurnitures.forEach((furniture) => {
  furniture.addEventListener("click", (e) => {
    document.querySelector(".details").classList.toggle("show");
    document
      .querySelectorAll(".index .column")
      .forEach((column) => column.classList.toggle("small-column"));
    closeDetail = !closeDetail;
    if (closeDetail && !closeCate && !closeAbout) {
      document.querySelector(".show-about > h2").innerHTML = "Close";
    } else if (closeDetail && closeCate) {
      document.querySelector(".category").classList.remove("show");
      document.querySelector(".show-category > h2").innerHTML = "Category";
      closeCate = false;
      document.querySelector(".show-about > h2").innerHTML = "Close";
    } else if (closeDetail && closeAbout) {
      document.querySelector(".about").classList.remove("show");
      document.querySelector(".show-about > h2").innerHTML = "about";
      closeAbout = false;
      document.querySelector(".show-about > h2").innerHTML = "Close";
    } else {
      document.querySelector(".show-about > h2").innerHTML = "About";
    }
  });
});

document.querySelectorAll(".prevent").forEach((a) => {
  a.addEventListener("click", (e) => {
    e.preventDefault();
  });
});
