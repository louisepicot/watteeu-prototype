const imgs = document.querySelectorAll(".details-images img");

let mouse = false;
function mouseStatus(n) {
  mouse = n;
}

let counter = 0;
document.addEventListener("mousemove", (e) => {
  if (mouse) {
    if (e.clientX >= 500) {
      // document.body.style.cursor = "../../assets/18.jpg";
      const elementToChange = document.getElementsByTagName("body")[0];
      elementToChange.style.cursor =
        "url('./assets/images/right-arrow.png'), auto";
    } else {
      const elementToChange = document.getElementsByTagName("body")[0];
      elementToChange.style.cursor =
        "url('./assets/images/left-arrow.png'), auto";
    }
  } else {
    const elementToChange = document.getElementsByTagName("body")[0];
    elementToChange.style.cursor = "";
  }
});

document.addEventListener("click", (e) => {
  if (mouse) {
    if (e.clientX >= 500) {
      // document.body.style.cursor = "../../assets/18.jpg";
      if (counter == imgs.length - 1) {
        imgs[imgs.length - 1].classList.remove("show");
        imgs[0].classList.add("show");
        counter = 0;
      } else {
        imgs[counter].classList.remove("show");
        imgs[counter + 1].classList.add("show");
        counter++;
      }
    } else {
      if (counter == 0) {
        imgs[0].classList.remove("show");
        imgs[imgs.length - 1].classList.add("show");
        counter = imgs.length - 1;
      } else {
        imgs[counter].classList.remove("show");
        imgs[counter - 1].classList.add("show");
        counter--;
      }
    }
  }
});

// function changeColor() {
//      if (/* the R key is pressed */) {
//           if (mouse) {
//                // change the color
//           }
//      }
// }
